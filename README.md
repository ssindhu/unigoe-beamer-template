# How to use this Template

Hi, welcome to the unigoe-beamer-template repo mirrored from
[Knut Zoch's template](https://github.com/knutzk/ugoe-template/blob/master/talk.tex) and slightly modified.

Most of the instructions on usage of this can be found in the `talk.pdf` you will create with this template!

To do that, simply do the following:

1. Download this repository (or clone/fork it, if you don't wanna miss out on git!)
2. `cd` into the repo folder.
3. Compile the code by hitting `make`. You will need a working LaTeX distribution and the packages required by the
   template, which can mostly be found in `beamerstyleugoe.tex`
4. Start creating your first talk. Simply throw out all of the template slides (but not the preamble!) for that.

## FSP Slides

For national conferences, such as the DPG, there now exists a separate corporate design. For this purpose, a second
template was kindly ported to LaTeX by Steffen Korn. It can be found in `fsp/`, while logos, figures, and package
imports in `beamerstyleugoe.tex` are handled in conjunction with the university template.

To compile the separate FSP template, use the following command (handled in the same way as above):

```shell
make fsp
```

## Abstract Slides

For some reason, Siegen 2022 wants abstracts to be submitted as slides. For this, there is now also a template
available. To test it, simply do the following:

1. Do the same thing as described above for the normal `talk.tex` template
2. Instead of compiling with `make` compile with `make abstract` instead. Now you should have a shiny new
   `abstract.pdf` file.
