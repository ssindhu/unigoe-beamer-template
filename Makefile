SRC = talk
SRC_FSP = talk_fsp
SRC_ABSTRACT = abstract

TMP = tmp
TMP_FSP = tmp_fsp
TMP_ABSTRACT = tmp_abstract

INKSCAPE = $(addsuffix .pdf, $(basename $(wildcard images/*.svg)))

.PHONY: all
.PHONY: abstract
.PHONY: clean
.PHONY: new
.PHONY: first-time
.PHONY: first-time-abstract
.PHONY: upload
.PHONY: compress
.PHONY: $(SRC).pdf
.PHONY: $(SRC_FSP).pdf
.PHONY: $(SRC_ABSTRACT).pdf

all: $(SRC).pdf

$(SRC).pdf: #$(SRC).tex $(INKSCAPE)
	test -d $(TMP) || $(MAKE) first-time
	lualatex \
		-output-directory $(TMP) \
		-halt-on-error \
		-file-line-error \
		$(SRC).tex
	-makeindex $(TMP)/$(SRC)
	-bibtex $(TMP)/$(SRC)
	cp $(TMP)/$(SRC).pdf .

fsp: $(SRC_FSP).pdf

$(SRC_FSP).pdf: #$(SRC).tex $(INKSCAPE)
	test -d $(TMP_FSP) || ($(MAKE) first-time-fsp; $(MAKE) first-time-fsp)
	cd fsp; \
	lualatex \
		-output-directory ../$(TMP_FSP) \
		-halt-on-error \
		-file-line-error \
		$(SRC_FSP).tex
	-makeindex $(TMP_FSP)/$(SRC_FSP)
	-bibtex $(TMP_FSP)/$(SRC_FSP)
	cp $(TMP_FSP)/$(SRC_FSP).pdf .

abstract: $(SRC_ABSTRACT).pdf

$(SRC_ABSTRACT).pdf: #$(SRC).tex $(INKSCAPE)
	test -d $(TMP_ABSTRACT) || $(MAKE) first-time-abstract
	lualatex \
		-output-directory $(TMP_ABSTRACT) \
		-halt-on-error \
		-file-line-error \
		$(SRC_ABSTRACT).tex
	-makeindex $(TMP_ABSTRACT)/$(SRC_ABSTRACT)
	-bibtex $(TMP_ABSTRACT)/$(SRC_ABSTRACT)
	cp $(TMP_ABSTRACT)/$(SRC_ABSTRACT).pdf .

compress:
	gs -dBATCH -dNOPAUSE -q -sDEVICE=pdfwrite -sOutputFile="$(SRC).pdf" "$(TMP)/$(SRC).pdf"

first-time:
	mkdir $(TMP)
	mkdir $(TMP)/gnuplot
	make $(SRC).pdf
	rm $(SRC).pdf

first-time-fsp:
	mkdir -p $(TMP_FSP)
	mkdir -p $(TMP_FSP)/gnuplot
	make $(SRC_FSP).pdf
	rm $(SRC_FSP).pdf

first-time-abstract:
	mkdir $(TMP_ABSTRACT)
	mkdir $(TMP_ABSTRACT)/gnuplot
	make $(SRC_ABSTRACT).pdf
	rm $(SRC_ABSTRACT).pdf

new: clean all

all-evince: all
	evince $(SRC).pdf > /dev/null 2>&1&

#%.pdf: %.eps
#	epstopdf $(basename $@).eps
#
#%.eps: %.svg
#	inkscape --export-area-drawing --without-gui $(basename $@).svg --export-eps=$(basename $@).eps
#%.pdf: %.svg
#	inkscape --export-area-drawing --without-gui $(basename $@).svg --export-pdf=$(basename $@).pdf
#	#inkscape --export-area-page --without-gui $(basename $@).svg --export-pdf=$(basename $@).pdf

clean:
	(test -e $(SRC).pdf && rm $(SRC).pdf) || :
	(test -d $(TMP) && rm -r $(TMP)) || :
	(test -e $(SRC_FSP).pdf && rm $(SRC_FSP).pdf) || :
	(test -d $(TMP_FSP) && rm -r $(TMP_FSP)) || :
	(test -e $(SRC_ABSTRACT).pdf && rm $(SRC_ABSTRACT).pdf) || :
	(test -d $(TMP_ABSTRACT) && rm -r $(TMP_ABSTRACT)) || :
#	-rm images/*.pdf
#	-rm images/*.eps
